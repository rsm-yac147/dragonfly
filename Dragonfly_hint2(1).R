library(igraph)

## for this example, we consider 6 activities, with the following depdendency structure.
## Start does not depend on anything
## A2 depends on Start
## A3 depends on Start and on A2
## A4 depends on Start, A2, and on A3. And so on...

mdata <- read.csv(text=
"Activity,Dependency,Best,Worst,Average,
Start,,1,5,2
A2,Start,1,5,2
A3,Start;A2,1,5,2
A4,Start;A2;A3,1,5,2
A5,Start;A3,1,5,2
End,Start;A2;A3;A4;A5,1,5,2", 
header=TRUE,stringsAsFactors = FALSE)

tfunc <- function(deplist,x){
  tmp <- unlist(strsplit(deplist,";"))
  matrix(c(tmp,rep(x,length(tmp))),byrow=FALSE,nrow=length(tmp))
}

e <- apply(subset(mdata,mdata$Dependency!=""),1,function(x){tfunc(x[2],x[1])})

e <- do.call('rbind',e)

##till the 'do.call' function above, we were simply generating the edge list from our data.
## we use this edge list to then make the graph


g <- graph_from_edgelist(e,directed=TRUE)

## from the graph, we find all simple paths starting from Start node and ending at End node

p <- all_simple_paths(g,from="Start",to="End")

plot(p)

## function below evaluates the length of a given path
path_length <- function(p2){
  sum(mdata$TIME[mdata$Activity %in% names(p2)])
}

## we are using runif to generate activity time for each of the activities.
## for the real simulation, you need to use rPERT

mdata$TIME <- apply(mdata,1,function(x){
  runif(1,as.numeric(x[3]),as.numeric(x[4]))
})

## this just finds the project duration for one particular run of the simulation.
## you need to also run it multiple times, each time generating new activity durations.

max( unlist( lapply(p,path_length) ) )
