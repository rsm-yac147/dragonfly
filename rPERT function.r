rPERT <- function( n, x.min, x.max, x.mode){
    if( x.min > x.max || x.mode > x.max || x.mode < x.min ) stop( "invalid parameters" );
    x.range <- x.max - x.min;
    if( x.range == 0 ) return( rep( x.min, n ));
    mu <- ( x.min + x.max + 4 * x.mode ) / ( 6 );
    # special case if mu == mode
    if( mu == x.mode ){
        v <- ( 4 / 2 ) + 1
    }
    else {
        v <- (( mu - x.min ) * ( 2 * x.mode - x.min - x.max )) /
            (( x.mode - mu ) * ( x.max - x.min ));
    }
    w <- ( v * ( x.max - mu )) / ( mu - x.min );
    return ( rbeta( n, v, w ) * x.range + x.min );
}

#The function rPERT has 4 arguments. The first argument is the number of samples you need, 
# the second x.min is the optimistic time estimate, x.max is the pessimistic time estimate, 
#and x.mode is the most likely time estimate.
#so if you call rPERT(10,2,10,8), it will generate 10 samples from the distribution where 
#the most optimistic time estimate is 2, pessimistic is 10, and most likely is 8.
#to see how the corresponding distribution looks like, look at the histogram as follows
#hist( rPERT(10000, 2, 10, 8) )
